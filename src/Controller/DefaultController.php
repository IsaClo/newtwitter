<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\User;
use App\Entity\Hashtag;
use App\Repository\HashtagRepository;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use App\Form\MessageType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{
    private $messageRepository;
    private $entityManager;
    private $userRepository;
    private $hashtagRepository;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository, MessageRepository $messageRepository, HashtagRepository $hashtagRepository)
    {
        $this->messageRepository=$messageRepository;
        $this->entityManager=$entityManager;
        $this->userRepository=$userRepository;
        $this->hashtagRepository=$hashtagRepository;
    }
    /**
     * @Route("/index", name="index")

     * @param $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function new(Request $request)
    {
        $messageList=$this->messageRepository->findAll();
        $hashtagList=$this->hashtagRepository->findAll();
        $message = new Message();

        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $message->setDate(new \DateTime());
            $message->setUser($this->getUser());
            $hashtags=$form->get('hashtags')->getData();
            $hash = explode("#", $hashtags);
            foreach ($hash as $i=>$key)
            {
                if ($i>0){
                    $listfact=$this->hashtagRepository->findOneBySomeField($key);
                    $req=count($listfact);
                    if ($req>0){
                        $message->addHashtag($listfact[0]);
                    } else {

                    $h=new Hashtag();
                    $h->setKeyword($key);
                    $message->addHashtag($h);
                    $this->entityManager->persist($h);
                    }
                }
            }

            $this->entityManager->persist($message);

            $this->entityManager->flush();
            return $this->redirectToRoute('index');


        }
        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
            'messages'=>$messageList,
            'hashtags'=>$hashtagList,
        ]);
    }

    /**
     * @Route("/profil/{id}", name="profil")

     */

    public function profil(int $id)
    {
        $pageprofil=$this->userRepository->find($id);
        $msguser=$pageprofil->getMessages();


        return $this->render('default/profil.html.twig', [
            'user'=>$pageprofil,
            'msguser'=>$msguser,
        ]);
    }

    /**
     * @Route("/hashtag/{id}", name="hashtag")

     */

    public function hashtag(int $id)
    {
        $pagehashtag=$this->hashtagRepository->find($id);
        $msguser=$pagehashtag->getMessage();


        return $this->render('default/hashtag.html.twig', [
            'hashtag'=>$pagehashtag,
            'msguser'=>$msguser,
        ]);
    }
}

