<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;



class FollowerController extends AbstractController
{

    private $entityManager;
    private $userRepository;

    function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository){

        $this->entityManager=$entityManager;
        $this->userRepository=$userRepository;
    }

    /**
     * @Route("/follow/{id}", name="follow")
     */
    public function followUser(int $id)
    {
        $moi=$this->getUser();
        $userFollow = $this->userRepository->find($id);
        $userFollow->addFollower($moi);
        $this->entityManager->persist($userFollow);
        $this->entityManager->persist($moi);
        $this->entityManager->flush();

        return $this->redirect('/profil/'.$userFollow->getId());

    }
}
